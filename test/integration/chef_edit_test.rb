require 'test_helper'

class ChefEditTest < ActionDispatch::IntegrationTest

  def setup
   @chef = Chef.create!(chefname: "mashrur",email: "mashrur@example.com",password: "password",password_confirmation: "password")
  @chef2 = Chef.create!(chefname: "johne",   email: "john@example.com",  password: "password", password_confirmation: "password")
  @admin = Chef.create!(chefname: "johnee", email: "johne@example.com",password: "password", password_confirmation: "password", admin: true)
  end

  test "reject an invalid edit" do
    sign_in_as(@chef,"password")
    get edit_chef_path(@chef)
    assert_template 'chefs/edit'
    patch chef_path(@chef), params: { chef: { chefname: " ",
                              email: "mashrur@example.com" } }
    assert_template 'chefs/edit'
    assert_select 'h3.panel-title'
    assert_select 'div.panel-body'
  end

  test "accept valid signup" do
    sign_in_as(@chef,"password")
    get edit_chef_path(@chef)
    assert_template 'chefs/edit'
    patch chef_path(@chef), params: { chef: { chefname: "mashrur1",
                                email: "mashrur1@example.com" } }
    assert_redirected_to @chef
    assert_not flash.empty?
    @chef.reload
    assert_match "mashrur1", @chef.chefname
    assert_match "mashrur1@example.com", @chef.email
  end

  test "accpet edit attempt by admin" do
    sign_in_as(@admin,"password")
    get edit_chef_path(@chef)
    assert_template 'chefs/edit'
    patch chef_path(@chef), params: { chef: { chefname: "mashrur3",
                                email: "mashrur3@example.com" } }
    assert_redirected_to @chef
    assert_not flash.empty?
    @chef.reload
    assert_match "mashrur3", @chef.chefname
    assert_match "mashrur3@example.com", @chef.email
  end

  test "redirect edit attempt of other by user" do
    sign_in_as(@chef2,"password")
    get edit_chef_path(@chef)
    patch chef_path(@chef), params: { chef: { chefname: "mashrur5",
                                email: "mashrur5@example.com" } }
    assert_redirected_to chefs_path
    assert_not flash.empty?
    @chef.reload
    assert_match "mashrur", @chef.chefname
    assert_match "mashrur@example.com", @chef.email
  end

end
