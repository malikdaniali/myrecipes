require 'test_helper'

class ChefSignupTest < ActionDispatch::IntegrationTest

  test "Chef shoud have signup path" do
    get signup_path
    assert_response :success
  end

  test "Reject Invalid Signup" do
    get signup_path
    assert_no_difference "Chef.count" do
      post chefs_path, params:{ chef: {chefname:"", email:"", password: "password", password_confirmation: "password"}}
    end
    assert_template 'chefs/new'
    assert_select 'h3.panel-title'
    assert_select 'div.panel-body'
  end

  test "Accept Valid Sign Up" do
    get signup_path
    assert_difference "Chef.count", 1 do
      post chefs_path, params:{ chef: {chefname:"Danish", email:"dani@example.com", password: "password", password_confirmation: "password"}}
    end
    follow_redirect!
    assert_template 'chefs/show'
    assert_not flash.empty?
  end
end
