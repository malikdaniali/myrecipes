require 'test_helper'

class ChefsShowTest < ActionDispatch::IntegrationTest

  def setup
    @chef = Chef.create!(chefname: "Danish", email: "malikdaniali@gmail.com", created_at: Time.now, password: "abc@123", password_confirmation: "abc@123")
    @recipe = Recipe.create(name: "Allo Matter", description: "Allo Matter are good", created_at: Time.now, chef: @chef )
    @recipe2 = @chef.recipes.build(name: "Allo Matter", description: "Allo Matter are good", created_at: Time.now)
    @recipe2.save
  end

  test "should get chef show" do
    sign_in_as(@chef,"password")
    get chef_path(@chef)
    assert_template 'chefs/show'
    assert_select "a[href=?]", recipe_path(@recipe), text: @recipe.name
    assert_match @recipe.description, response.body
    assert_match @chef.chefname, response.body
  end




end
