require 'test_helper'

class RecipeTest < ActiveSupport::TestCase

  def setup
    @chef = Chef.create!(chefname: "Danish", email: "danish@example.com", created_at: Time.now, password: "abc@123", password_confirmation: "abc@123")
    @recipe = @chef.recipes.build(name: "Vegetable", description: "Great Vegetable", created_at: Time.now)
  end

  test "recipes without chef should be invalid" do
    @recipe.chef_id = nil
    assert_not @recipe.valid?
  end


  test "recipe should be valid" do
    assert @recipe.valid?
  end

  test "description should not be less than 10 charcters" do
    @recipe.description = "a" * 9
    assert_not @recipe.valid?
  end

  test "description should not be more than 500 charcters" do
    @recipe.description = "a" * 501
    assert_not @recipe.valid?
  end
end
