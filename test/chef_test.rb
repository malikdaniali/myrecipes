 require 'test_helper'

class ChefTest < ActiveSupport::TestCase

  def setup
    @chef = Chef.new(chefname: "Danish", email: "abc@example.com", created_at: Time.now, password: "abc@123", password_confirmation: "abc@123")
  end

  test "Chef should be valid" do
  assert @chef.valid?
  end


  test "Name should be presence" do
    @chef.chefname = " "
    assert_not @chef.valid?
  end

  test "Name should be less than 255 charcters" do
    @chef.chefname = "a" * 256
    assert_not @chef.valid?
  end

  test "email should be accept correct format" do
    valid_emails = %w[user@example.com MALIKDANIALI@gmail.com M.First@yahoo.com jon_snow@got.com]
    valid_emails.each do| valids |
    @chef.email = valids
    assert @chef.valid?, "#{valids.inspect} should be valid"
    end
  end

  test "Should reject invalid email address" do

    invalid_emails = %w[user@example MALIKDANIALI.com M.First]
    invalid_emails.each do | invalids |
    @chef.email = invalids
    assert_not @chef.valid?, "#{invalids.inspect} should be invalid"
    end
  end

  test "should be unique" do
    dulplicate_chef = @chef.dup
    dulplicate_chef.email = @chef.email.upcase
    dulplicate_chef.save
    assert dulplicate_chef.valid?
    end

  test "test should be lower case before hitting db" do
    mixed_email = "User@Example.Com"
    @chef.email = mixed_email
    @chef.save
    assert_equal mixed_email.downcase, @chef.reload.email
  end

   test "password should be present" do
     @chef.password = @chef.password_confirmation = " "
     assert_not @chef.valid?
   end

   test "password should be atleast 6 charcters" do
     @chef.password = @chef.password_confirmation = " "
     assert_not @chef.valid?
   end

   test "associated recipes should be deleted" do
     @chef.save
     @chef.recipes.create!(name: "Testing destroy", description: "Testing destroy description", created_at: Time.now)
     assert_difference 'Recipe.count', -1 do
       @chef.destroy
     end
   end

end
