class ChefsController < ApplicationController

  before_action :set_chef, only: [:show, :edit, :update, :destroy]
  before_action :require_same_user, only: [:edit, :update, :destroy]
  before_action :require_admin, only: [:destroy]

  def index
    @chefs = Chef.paginate(page: params[:page], per_page: 5)
  end

  def new
    @chef = Chef.new
  end

  def create
    @chef = Chef.new(chef_params)
    @chef.created_at= Time.now
    if @chef.save
      session[:chef_id] = @chef.id
      flash[:success] = "Welcome #{@chef.chefname} to application"
      redirect_to chef_path(@chef)
    else
    render 'new'
    end
    #end of func
  end

  def show
    @chef_recipes = @chef.recipes.paginate(page: params[:page], per_page: 5)
  end



  def edit

  end

  def update
    if @chef.update(chef_params)
      flash[:success] = "Your account was update successfully"
      redirect_to @chef
    else
      render 'edit'
    end
    #end of func
    end


    def destroy
      @chef.destroy
      flash[:danger] = "Chef and all associated recipes have been deleted"
      redirect_to chefs_path
    end

    private
    def chef_params
      params.require(:chef).permit(:chefname, :email, :password, :password_confirmation)
    #end of func
    end

    def set_chef
        @chef = Chef.find(params[:id])
    end

    def require_admin
      if logged_in? && !current_chef.admin?
        flash[:success] = "Only admin can perform this action"
        redirect_to root_path
      end
    end

    def require_same_user
      if current_chef != @chef && !current_chef.admin?
        flash[:danger] = "You can edit or delete only your acount"
        redirect_to chefs_path
      end
    end
end
