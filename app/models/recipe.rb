class Recipe < ApplicationRecord

validates :name, presence: true
validates :description, presence: true, length: {minimum: 10, maximum: 500}
validates :created_at, presence: true

belongs_to :chef
validates :chef_id, presence: true

default_scope -> { order(created_at: :desc)}

end
